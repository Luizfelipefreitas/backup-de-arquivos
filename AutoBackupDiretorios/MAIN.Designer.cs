﻿
namespace AutoBackupDiretorios
{
    partial class MAIN
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_salvar_configuracoes = new System.Windows.Forms.Button();
            this.txb_caminho_origem = new System.Windows.Forms.TextBox();
            this.txb_caminho_backup = new System.Windows.Forms.TextBox();
            this.lbl_caminho_arquivo = new System.Windows.Forms.Label();
            this.lbl_caminho_backup = new System.Windows.Forms.Label();
            this.gpb_configuracoes = new System.Windows.Forms.GroupBox();
            this.pgb_em_exec = new System.Windows.Forms.ProgressBar();
            this.cb_apagar_arquivos_origem = new System.Windows.Forms.CheckBox();
            this.btn_procurar_caminho_destino = new System.Windows.Forms.Button();
            this.btn_procurar_caminho_origem = new System.Windows.Forms.Button();
            this.cb_apagar_diretorios_e_arquivos_origem = new System.Windows.Forms.CheckBox();
            this.btn_executar_rotina = new System.Windows.Forms.Button();
            this.rtb_saida_cmd = new System.Windows.Forms.RichTextBox();
            this.lbl_log_saida_cmd = new System.Windows.Forms.Label();
            this.fbd_caminho_origem = new System.Windows.Forms.FolderBrowserDialog();
            this.fbd_caminho_destino = new System.Windows.Forms.FolderBrowserDialog();
            this.timer_pgb_exec = new System.Windows.Forms.Timer(this.components);
            this.gpb_configuracoes.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_salvar_configuracoes
            // 
            this.btn_salvar_configuracoes.Location = new System.Drawing.Point(9, 136);
            this.btn_salvar_configuracoes.Name = "btn_salvar_configuracoes";
            this.btn_salvar_configuracoes.Size = new System.Drawing.Size(75, 21);
            this.btn_salvar_configuracoes.TabIndex = 0;
            this.btn_salvar_configuracoes.Text = "Salvar";
            this.btn_salvar_configuracoes.UseVisualStyleBackColor = true;
            this.btn_salvar_configuracoes.Click += new System.EventHandler(this.btn_salvar_configuracoes_Click);
            // 
            // txb_caminho_origem
            // 
            this.txb_caminho_origem.Location = new System.Drawing.Point(6, 38);
            this.txb_caminho_origem.Name = "txb_caminho_origem";
            this.txb_caminho_origem.Size = new System.Drawing.Size(527, 23);
            this.txb_caminho_origem.TabIndex = 1;
            // 
            // txb_caminho_backup
            // 
            this.txb_caminho_backup.Location = new System.Drawing.Point(6, 82);
            this.txb_caminho_backup.Name = "txb_caminho_backup";
            this.txb_caminho_backup.Size = new System.Drawing.Size(527, 23);
            this.txb_caminho_backup.TabIndex = 2;
            // 
            // lbl_caminho_arquivo
            // 
            this.lbl_caminho_arquivo.AutoSize = true;
            this.lbl_caminho_arquivo.Location = new System.Drawing.Point(6, 20);
            this.lbl_caminho_arquivo.Name = "lbl_caminho_arquivo";
            this.lbl_caminho_arquivo.Size = new System.Drawing.Size(482, 15);
            this.lbl_caminho_arquivo.TabIndex = 3;
            this.lbl_caminho_arquivo.Text = "Informe o caminho do arquivo a ser salvo (Se a pasta nao existir o sistema nao fa" +
    "z backup)";
            // 
            // lbl_caminho_backup
            // 
            this.lbl_caminho_backup.AutoSize = true;
            this.lbl_caminho_backup.Location = new System.Drawing.Point(6, 64);
            this.lbl_caminho_backup.Name = "lbl_caminho_backup";
            this.lbl_caminho_backup.Size = new System.Drawing.Size(413, 15);
            this.lbl_caminho_backup.TabIndex = 4;
            this.lbl_caminho_backup.Text = "Informe o destino do arquivo a ser salvo (Se a pasta nao existir o sistema cria)";
            // 
            // gpb_configuracoes
            // 
            this.gpb_configuracoes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpb_configuracoes.Controls.Add(this.pgb_em_exec);
            this.gpb_configuracoes.Controls.Add(this.cb_apagar_arquivos_origem);
            this.gpb_configuracoes.Controls.Add(this.btn_procurar_caminho_destino);
            this.gpb_configuracoes.Controls.Add(this.btn_procurar_caminho_origem);
            this.gpb_configuracoes.Controls.Add(this.cb_apagar_diretorios_e_arquivos_origem);
            this.gpb_configuracoes.Controls.Add(this.btn_executar_rotina);
            this.gpb_configuracoes.Controls.Add(this.lbl_caminho_backup);
            this.gpb_configuracoes.Controls.Add(this.lbl_caminho_arquivo);
            this.gpb_configuracoes.Controls.Add(this.txb_caminho_backup);
            this.gpb_configuracoes.Controls.Add(this.txb_caminho_origem);
            this.gpb_configuracoes.Controls.Add(this.btn_salvar_configuracoes);
            this.gpb_configuracoes.Location = new System.Drawing.Point(3, 2);
            this.gpb_configuracoes.Name = "gpb_configuracoes";
            this.gpb_configuracoes.Size = new System.Drawing.Size(785, 201);
            this.gpb_configuracoes.TabIndex = 5;
            this.gpb_configuracoes.TabStop = false;
            this.gpb_configuracoes.Text = "Configurações";
            // 
            // pgb_em_exec
            // 
            this.pgb_em_exec.Location = new System.Drawing.Point(286, 134);
            this.pgb_em_exec.Name = "pgb_em_exec";
            this.pgb_em_exec.Size = new System.Drawing.Size(100, 23);
            this.pgb_em_exec.TabIndex = 10;
            // 
            // cb_apagar_arquivos_origem
            // 
            this.cb_apagar_arquivos_origem.AutoSize = true;
            this.cb_apagar_arquivos_origem.Location = new System.Drawing.Point(300, 111);
            this.cb_apagar_arquivos_origem.Name = "cb_apagar_arquivos_origem";
            this.cb_apagar_arquivos_origem.Size = new System.Drawing.Size(348, 19);
            this.cb_apagar_arquivos_origem.TabIndex = 9;
            this.cb_apagar_arquivos_origem.Text = "Apagar arquivos na origem (So exclui arquivos e nao pastas)?";
            this.cb_apagar_arquivos_origem.UseVisualStyleBackColor = true;
            // 
            // btn_procurar_caminho_destino
            // 
            this.btn_procurar_caminho_destino.Location = new System.Drawing.Point(539, 82);
            this.btn_procurar_caminho_destino.Name = "btn_procurar_caminho_destino";
            this.btn_procurar_caminho_destino.Size = new System.Drawing.Size(75, 23);
            this.btn_procurar_caminho_destino.TabIndex = 8;
            this.btn_procurar_caminho_destino.Text = "Procurar";
            this.btn_procurar_caminho_destino.UseVisualStyleBackColor = true;
            this.btn_procurar_caminho_destino.Click += new System.EventHandler(this.btn_procurar_caminho_destino_Click);
            // 
            // btn_procurar_caminho_origem
            // 
            this.btn_procurar_caminho_origem.Location = new System.Drawing.Point(539, 38);
            this.btn_procurar_caminho_origem.Name = "btn_procurar_caminho_origem";
            this.btn_procurar_caminho_origem.Size = new System.Drawing.Size(75, 23);
            this.btn_procurar_caminho_origem.TabIndex = 7;
            this.btn_procurar_caminho_origem.Text = "Procurar";
            this.btn_procurar_caminho_origem.UseVisualStyleBackColor = true;
            this.btn_procurar_caminho_origem.Click += new System.EventHandler(this.btn_procurar_caminho_origem_Click);
            // 
            // cb_apagar_diretorios_e_arquivos_origem
            // 
            this.cb_apagar_diretorios_e_arquivos_origem.AutoSize = true;
            this.cb_apagar_diretorios_e_arquivos_origem.Location = new System.Drawing.Point(10, 111);
            this.cb_apagar_diretorios_e_arquivos_origem.Name = "cb_apagar_diretorios_e_arquivos_origem";
            this.cb_apagar_diretorios_e_arquivos_origem.Size = new System.Drawing.Size(284, 19);
            this.cb_apagar_diretorios_e_arquivos_origem.TabIndex = 6;
            this.cb_apagar_diretorios_e_arquivos_origem.Text = "Apagar pastas na origem (Incluindo a pasta raiz)?";
            this.cb_apagar_diretorios_e_arquivos_origem.UseVisualStyleBackColor = true;
            this.cb_apagar_diretorios_e_arquivos_origem.CheckedChanged += new System.EventHandler(this.cb_apagar_diretorios_e_arquivos_origem_CheckedChanged);
            // 
            // btn_executar_rotina
            // 
            this.btn_executar_rotina.Location = new System.Drawing.Point(192, 134);
            this.btn_executar_rotina.Name = "btn_executar_rotina";
            this.btn_executar_rotina.Size = new System.Drawing.Size(75, 23);
            this.btn_executar_rotina.TabIndex = 5;
            this.btn_executar_rotina.Text = "Executar";
            this.btn_executar_rotina.UseVisualStyleBackColor = true;
            this.btn_executar_rotina.Click += new System.EventHandler(this.btn_executar_rotina_Click);
            // 
            // rtb_saida_cmd
            // 
            this.rtb_saida_cmd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtb_saida_cmd.Location = new System.Drawing.Point(13, 193);
            this.rtb_saida_cmd.Name = "rtb_saida_cmd";
            this.rtb_saida_cmd.Size = new System.Drawing.Size(775, 278);
            this.rtb_saida_cmd.TabIndex = 6;
            this.rtb_saida_cmd.Text = "";
            // 
            // lbl_log_saida_cmd
            // 
            this.lbl_log_saida_cmd.AutoSize = true;
            this.lbl_log_saida_cmd.Location = new System.Drawing.Point(13, 175);
            this.lbl_log_saida_cmd.Name = "lbl_log_saida_cmd";
            this.lbl_log_saida_cmd.Size = new System.Drawing.Size(90, 15);
            this.lbl_log_saida_cmd.TabIndex = 7;
            this.lbl_log_saida_cmd.Text = "LOG saida CMD";
            // 
            // fbd_caminho_origem
            // 
            this.fbd_caminho_origem.SelectedPath = "C:\\origem";
            // 
            // fbd_caminho_destino
            // 
            this.fbd_caminho_destino.SelectedPath = "C:\\backup";
            // 
            // timer_pgb_exec
            // 
            this.timer_pgb_exec.Interval = 1000;
            this.timer_pgb_exec.Tick += new System.EventHandler(this.timer_pgb_exec_Tick);
            // 
            // MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 483);
            this.Controls.Add(this.lbl_log_saida_cmd);
            this.Controls.Add(this.rtb_saida_cmd);
            this.Controls.Add(this.gpb_configuracoes);
            this.Name = "MAIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de backup de arquivos";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gpb_configuracoes.ResumeLayout(false);
            this.gpb_configuracoes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_salvar_configuracoes;
        private System.Windows.Forms.TextBox txb_caminho_origem;
        private System.Windows.Forms.TextBox txb_caminho_backup;
        private System.Windows.Forms.Label lbl_caminho_arquivo;
        private System.Windows.Forms.Label lbl_caminho_backup;
        private System.Windows.Forms.GroupBox gpb_configuracoes;
        private System.Windows.Forms.RichTextBox rtb_saida_cmd;
        private System.Windows.Forms.CheckBox cb_apagar_diretorios_e_arquivos_origem;
        private System.Windows.Forms.Button btn_executar_rotina;
        private System.Windows.Forms.Label lbl_log_saida_cmd;
        private System.Windows.Forms.FolderBrowserDialog fbd_caminho_origem;
        private System.Windows.Forms.Button btn_procurar_caminho_destino;
        private System.Windows.Forms.Button btn_procurar_caminho_origem;
        private System.Windows.Forms.FolderBrowserDialog fbd_caminho_destino;
        private System.Windows.Forms.CheckBox cb_apagar_arquivos_origem;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ProgressBar pgb_em_exec;
        private System.Windows.Forms.Timer timer_pgb_exec;
    }
}

