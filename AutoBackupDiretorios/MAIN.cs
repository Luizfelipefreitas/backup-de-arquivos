﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoBackupDiretorios
{
    public partial class MAIN : Form
    {
        public MAIN()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //caminho dos arquivos a serem salvos 
            txb_caminho_origem.Text = Properties.Settings.Default.CaminhoArquivo;
            txb_caminho_backup.Text = Properties.Settings.Default.CaminhoBackup;
            cb_apagar_diretorios_e_arquivos_origem.Checked = Properties.Settings.Default.ParametroApagarDiretoriosEArquivosOrigem;
            cb_apagar_arquivos_origem.Checked = Properties.Settings.Default.ParametroApagarDiretoriosEArquivosOrigem;

            //barra de progresso
            this.pgb_em_exec.Maximum = 100;
            pgb_em_exec.Visible = false;
        }

        private void Rotina_Backup ()
        {
            btn_executar_rotina.Enabled = false;
            pgb_em_exec.Value = 0;
            pgb_em_exec.Visible = true;
            
            //timer para comecar a rolar o progresso da barra
            this.timer_pgb_exec.Start();

            string move = string.Empty;
            //pega o caminho do sistema em execucao
            String path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
            string Date = DateTime.Now.ToString("dd-MM-yyyy");
            string ArquivoLOG = path + "bkp_" + Date + ".log";

            if (cb_apagar_diretorios_e_arquivos_origem.Checked == true && cb_apagar_arquivos_origem.Checked == true)
            {
                move = "/move";
            }
            
            if (cb_apagar_arquivos_origem.Checked == true && cb_apagar_diretorios_e_arquivos_origem.Checked == false)
            {
                move = "/mov";
            }
            
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = @"/C robocopy " + "\"" + txb_caminho_origem.Text + "\" \"" + txb_caminho_backup.Text + "\" /E /COPYALL /ZB /R:3 /W:10 /XO /ETA /secfix " + move + " /LOG+:\"" + ArquivoLOG + "\"";
            startInfo.CreateNoWindow = true;
            //startInfo.RedirectStandardOutput = true; //disponibiliza a saida do cmd
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();

            //rtb_saida_cmd.Text = process.StandardOutput.ReadToEnd(); //escreve a saida do cmd na tela do form

            StreamReader tr = new StreamReader(ArquivoLOG);
            rtb_saida_cmd.Text = tr.ReadToEnd();
            tr.Close();

            
            MessageBox.Show("Processo executado!");
            timer_pgb_exec.Stop();
            btn_executar_rotina.Enabled = true;
            pgb_em_exec.Visible = false;
        }

        private void btn_salvar_configuracoes_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.CaminhoArquivo = txb_caminho_origem.Text;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.CaminhoBackup = txb_caminho_backup.Text;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.ParametroApagarDiretoriosEArquivosOrigem = cb_apagar_diretorios_e_arquivos_origem.Checked;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.ParametroApagarArquivosOrigem = cb_apagar_arquivos_origem.Checked;
            Properties.Settings.Default.Save();

            MessageBox.Show("Configuracoes salvas com sucesso!");

        }

        private void btn_executar_rotina_Click(object sender, EventArgs e)
        {
            Mensagem_confirma_caminho();
        }

        private void btn_procurar_caminho_origem_Click(object sender, EventArgs e)
        {
            if(fbd_caminho_origem.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txb_caminho_origem.Text = fbd_caminho_origem.SelectedPath;
            }

        }

        private void btn_procurar_caminho_destino_Click(object sender, EventArgs e)
        {
            if (fbd_caminho_destino.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txb_caminho_backup.Text = fbd_caminho_destino.SelectedPath;
            }
        }

        private void Mensagem_confirma_caminho()
        {
            DialogResult confirm = MessageBox.Show("Os caminhos de origem e destino do BACKUP estao certos?", "Confirmacao de caminho", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

            if (confirm.ToString().ToUpper() == "YES")
            {
                Rotina_Backup();
            }

            if (confirm.ToString().ToUpper() == "NO")
            {
                if (fbd_caminho_origem.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txb_caminho_origem.Text = fbd_caminho_origem.SelectedPath;
                }

                if (fbd_caminho_destino.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txb_caminho_backup.Text = fbd_caminho_destino.SelectedPath;
                }

                Rotina_Backup();
            }
        }

        private void cb_apagar_diretorios_e_arquivos_origem_CheckedChanged(object sender, EventArgs e)
        {
            cb_apagar_arquivos_origem.Checked = true;
        }

        private void timer_pgb_exec_Tick(object sender, EventArgs e)
        {
            this.pgb_em_exec.Increment(1);
            if(pgb_em_exec.Value >= 100)
            {
                pgb_em_exec.Value = 0;
            }
        }
    }
}
